﻿using CafeLib.Core.IoC;
using MvvmDemo.ViewModels;

namespace MvvmDemo
{
    public static class Bootstrapper
    {
        /// <summary>
        /// Initialize the Navigation Page here.
        /// </summary>
        /// <returns>navigation page</returns>
        /// <param name="registry"></param>
        public static void InitApplication(IServiceRegistry registry)
        {
            registry.AddSingleton<PlaylistsViewModel, PlaylistsViewModel>();
            registry.AddTransient<PlaylistDetailViewModel, PlaylistDetailViewModel>();
        }
    }
}
