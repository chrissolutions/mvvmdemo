﻿using System;
using System.Diagnostics;
using CafeLib.Core.IoC;
using MvvmDemo.ViewModels;
using Xamarin.Forms;

namespace MvvmDemo
{
    // ReSharper disable once RedundantExtendsListEntry
    public partial class App : Application
	{
        private readonly IServiceRegistry _registry;

        public App (IServiceRegistry registry)
        {
            _registry = registry;
			InitializeComponent();
		}

		protected override void OnStart ()
		{
		    try
		    {
		        Bootstrapper.InitApplication(_registry);

                // Initialize the root page.
                MainPage = _registry.GetResolver().Resolve<PlaylistsViewModel>().AsNavigationPage();
            }
            catch (Exception e)
		    {
		        Debug.WriteLine(e);
		        throw;
		    }
		    // Handle when your app starts
        }

        protected override void OnSleep ()
		{
			// Handle when your app sleeps
		}

		protected override void OnResume ()
		{
			// Handle when your app resumes
		}
	}
}
